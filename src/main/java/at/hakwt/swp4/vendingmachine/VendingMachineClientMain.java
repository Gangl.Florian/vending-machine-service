package at.hakwt.swp4.vendingmachine;

public class VendingMachineClientMain {

    public static void main(String[] args) {
        // applicationContext.getBean("vendingMachineService", VendingMachineService.class);
        VendingMachineService service = new DefaultVendingMachineService();

        for (int i = 0; i < 10; i++) {
            service.drinkSold("Cola 0.3", 1.99d);
            service.drinkSold("Römerquelle 0.5", 0.99d);
        }
        System.out.println("Heutiger Umsatz: " + service.getDailyRevenue());


    }

}
