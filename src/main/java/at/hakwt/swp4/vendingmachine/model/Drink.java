package at.hakwt.swp4.vendingmachine.model;

public class Drink {

    private String name;

    public Drink(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
