package at.hakwt.swp4.vendingmachine.storage;

import at.hakwt.swp4.vendingmachine.model.Drink;

import java.util.ArrayList;
import java.util.List;

public class AustrianDrinkStorage implements DrinkStorageDao {

    @Override
    public List<Drink> getDrinks(String name, int amount) {
        List<Drink> drinks = new ArrayList<>(amount);
        for (int i = 0; i < amount; i++) {
            drinks.add(new Drink(name));
        }
        return drinks;
    }

}
