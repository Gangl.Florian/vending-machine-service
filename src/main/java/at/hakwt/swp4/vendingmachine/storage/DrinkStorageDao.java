package at.hakwt.swp4.vendingmachine.storage;

import at.hakwt.swp4.vendingmachine.model.Drink;

import java.util.List;

public interface DrinkStorageDao {

    List<Drink> getDrinks(String name, int amount);

}
